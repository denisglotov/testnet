#!/bin/bash
set -ue

geth --datadir data --nodiscover --syncmode full --mine --rpc --rpcaddr 0.0.0.0 \
     --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3 --rpccorsdomain "*" --networkid 17
     # --unlock 0 --password password.txt
