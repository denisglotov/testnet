#!/bin/bash
set -ue

rm -rf data/
mkdir -p data/keystore
cp key.c82f9a40 data/keystore/key
geth --datadir data init genesis.json
