Ethereum testnet
================

First, you need to install `geth` client, that you want to try.

You may follow these instructions to take the latest one.
https://github.com/ethereum/go-ethereum/wiki/Installation-Instructions-for-Ubuntu.

Then run:

    init.sh
    start.sh

You may connect to the started client from another terminal:

    geth attach data/geth.ipc

Note, that [genesis.json](./genesis.json) is taken from the [private network]
wiki page so it is good to update it from there from time to time.

[private network]: https://github.com/ethereum/go-ethereum/wiki/Private-network
